package com.aadi.tucwlan.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.aadi.tucwlan.R;
import com.aadi.tucwlan.fragments.OnBoardingFragment;
import com.aadi.tucwlan.fragments.OnBoardingLastFragment;

public class OnBoardingActivity extends AppCompatActivity {

    private ViewPager mViewPager;
    OnBoardingPagerAdapter mPagerAdapter;

    int page = 0;
    private ImageView[] indicators;
    private Button btn_login, btn_skip;
    private ImageButton btn_next;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarding);

        indicators = new ImageView[]{
                ((ImageView) findViewById(R.id.onboarding_indicator_0)),
                ((ImageView) findViewById(R.id.onboarding_indicator_1)),
                ((ImageView) findViewById(R.id.onboarding_indicator_2)),
                ((ImageView) findViewById(R.id.onboarding_indicator_3))
        };

        btn_login = (Button) findViewById(R.id.onboarding_btn_login);
        btn_next = (ImageButton) findViewById(R.id.onboarding_btn_next);
        btn_skip = (Button) findViewById(R.id.onboarding_btn_skip);

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page += 1;
                mViewPager.setCurrentItem(page, true);
            }
        });

        btn_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page = 3;
                mViewPager.setCurrentItem(page, true);
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (OnBoardingLastFragment.isSecure && OnBoardingLastFragment.isConnected) {
                    Intent intent = new Intent(OnBoardingActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
                else {
                    showCheckRequirementDialog(v);
                }
            }
        });

        mPagerAdapter = new OnBoardingPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mPagerAdapter);

        mViewPager.setCurrentItem(page);
        updateIndicators(page);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                updateIndicators(position);

                btn_next.setVisibility(position == 3 ? View.GONE : View.VISIBLE);
                btn_login.setVisibility(position == 3 ? View.VISIBLE : View.GONE);
                btn_skip.setVisibility(position == 3 ? View.GONE : View.VISIBLE);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void showCheckRequirementDialog(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
        builder.setTitle(R.string.onboarding_requirement_check_dialog_title);
        builder.setMessage(R.string.onboarding_requirement_check_dialog_msg);
        builder.setPositiveButton(R.string.ok, null);
        builder.setNegativeButton(R.string.cancel, null);
        builder.show();
    }

    void updateIndicators(int position) {
        for (int i = 0; i < indicators.length; i++) {
            indicators[i].setBackgroundResource(
                    i == position ? R.drawable.indicator_selected : R.drawable.indicator_unselected
            );
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public static class OnBoardingPagerAdapter extends FragmentPagerAdapter {

        OnBoardingPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {

            if(position == 3)
                return OnBoardingLastFragment.newInstance();
            else
                return OnBoardingFragment.newInstance(position);

        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0: return "SECTION 1";
                case 1: return "SECTION 2";
                case 2: return "SECTION 3";
                case 3: return "SECTION 4";
            }
            return null;
        }

    }

}


