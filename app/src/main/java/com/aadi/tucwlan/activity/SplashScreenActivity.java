package com.aadi.tucwlan.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;

import com.aadi.tucwlan.MainActivity;
import com.aadi.tucwlan.utils.Helper;

public class SplashScreenActivity extends Activity {

    private static final String TAG = SplashScreenActivity.class.getName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Boolean isNewUser = Boolean.valueOf(Helper.readSharedPreferences(
                SplashScreenActivity.this,
                Helper.SHARED_PREF_NEW_USER,
                "true"));

        Log.v(TAG, "New User: " + isNewUser);

        Intent intent;
        if(isNewUser){
            intent = new Intent(SplashScreenActivity.this, OnBoardingActivity.class);
        }
        else {
            intent = new Intent(SplashScreenActivity.this, MainActivity.class);
        }
        startActivity(intent);
        finish();
    }
}
