package com.aadi.tucwlan.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import com.aadi.tucwlan.R;

public class NoConfiguration extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_configurations);

        String username = getIntent().getStringExtra("USERNAME");

        ((TextView) findViewById(R.id.logged_in_as)).setText(getResources().getString(R.string.message_logged_in_as, username));

        ((AppCompatButton)findViewById(R.id.button_title)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NoConfiguration.this, NewConfiguration.class);
                startActivity(intent);
            }
        });
    }
}
