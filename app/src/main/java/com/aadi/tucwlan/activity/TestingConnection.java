package com.aadi.tucwlan.activity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.NetworkRequest;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.aadi.tucwlan.R;
import com.aadi.tucwlan.utils.Helper;
import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieDrawable;

public class TestingConnection extends AppCompatActivity {

    final private String TAG = TestingConnection.class.getName();

    private LottieAnimationView animationView;
    private TextView titleView;
    private TextView subtitleView;
    private TextView caption_view;
    private Button button_1;
    private Button button_2;

    final private int CONNECTED = 1;
    final private int CONNECTED_TO_OTHER_NETWORK = 3;
    final private int CONNECTED_MOBILE_NETWORK = 2;
    final private int EDUROAM_NO_INTERNET = 0;
    final private int EDUROAM_NETWORK_NOT_ENABLED = -2;
    final private int EDUROAM_NETWORK_NOT_FOUND = -1;
    final private int TESTING_CONNECTION = -3;
    final private int LOCATION_PERMISSION = -4;

    private Handler mHandler = new Handler();
    private int mInterval = 1000;
    private int count = 0;
    private String lastAddedWifi;
    private String connected_ssid =  "null";
    private ConnectivityManager cm;
    private ConnectivityManager.NetworkCallback networkCallback;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setLayoutDesign();

    }

    private void setLayoutDesign() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        animationView = (LottieAnimationView) findViewById(R.id.animation_view);
        titleView = (TextView) findViewById(R.id.title_view);
        subtitleView = (TextView) findViewById(R.id.subtitle_view);
        caption_view = (TextView) findViewById(R.id.caption_view);
        button_1 = (Button) findViewById(R.id.button_1);
        button_2 = (Button) findViewById(R.id.button_2);

        animationView.setAnimation("loading_animation.json");
        animationView.playAnimation();
        animationView.setRepeatCount(LottieDrawable.INFINITE);
        titleView.setText(getResources().getString(R.string.testing_connection_title));
        caption_view.setText(getResources().getString(R.string.testing_connection_subtitle));
        subtitleView.setVisibility(View.GONE);
        titleView.setVisibility(View.VISIBLE);
        caption_view.setVisibility(View.VISIBLE);
        button_1.setVisibility(View.VISIBLE);
        button_2.setVisibility(View.INVISIBLE);

        button_1.setText(getResources().getString(R.string.exit));
        button_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TestingConnection.this.finish();
            }
        });

        button_2.setText(getResources().getString(R.string.reconfigure));
        button_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TestingConnection.this, LoginActivity.class));
            }
        });

        startTestingConnection();
    }

    void startTestingConnection(){
        mTestingConnection.run();
    }

    Runnable mTestingConnection = new Runnable() {
        @Override
        public void run() {
            try {
                int test = testConnection();
                if(test == CONNECTED && count > 3) {
                    titleView.setText(getResources().getString(R.string.msg_connection_established_success));
                    animationView.setImageDrawable(ContextCompat.getDrawable(TestingConnection.this, R.drawable.ic_check_circle));
                    stopTestingConnection();
                    button_1.setText(getResources().getString(R.string.exit));

                } else if(test == CONNECTED_MOBILE_NETWORK && count > 3) {
                    titleView.setText(getResources().getString(R.string.msg_connected_mobile_network));
                    caption_view.setText(getResources().getString(R.string.testing_connection_subtitle));
                    animationView.setImageDrawable(ContextCompat.getDrawable(TestingConnection.this, R.drawable.ic_error));
                    button_1.setText(getResources().getString(R.string.exit));
                    stopTestingConnection();

                } else if(test == CONNECTED_TO_OTHER_NETWORK && count > 3) {
                    titleView.setText(getResources().getString(R.string.msg_connected_another_network));
                    caption_view.setText(getResources().getString(R.string.testing_connection_subtitle));
                    animationView.setImageDrawable(ContextCompat.getDrawable(TestingConnection.this, R.drawable.ic_error));
                    button_1.setText(getResources().getString(R.string.exit));
                    stopTestingConnection();

                } else if(test == EDUROAM_NO_INTERNET && count > 8) {
                    titleView.setText(getResources().getString(R.string.msg_connection_failed));
                    caption_view.setText(getResources().getString(R.string.msg_eduroam_no_internet));
                    animationView.setImageDrawable(ContextCompat.getDrawable(TestingConnection.this, R.drawable.ic_error));
                    button_1.setText(getResources().getString(R.string.exit));
                    stopTestingConnection();

                } else if(test == EDUROAM_NETWORK_NOT_ENABLED && count > 8) {
                    titleView.setText(getResources().getString(R.string.error));
                    subtitleView.setVisibility(View.VISIBLE);
                    subtitleView.setText(getResources().getString(R.string.msg_eduroam_not_enabled));
                    caption_view.setText(getResources().getString(R.string.caption_eduroam_not_enabled));
                    animationView.setImageDrawable(ContextCompat.getDrawable(TestingConnection.this, R.drawable.ic_error));
                    button_1.setVisibility(View.GONE);
                    button_2.setVisibility(View.VISIBLE);
                    stopTestingConnection();

                } else if(test == EDUROAM_NETWORK_NOT_FOUND && count > 8) {
                    titleView.setText(getResources().getString(R.string.title_eduroam_not_found));
                    caption_view.setText(getResources().getString(R.string.msg_eduroam_not_foud));
                    animationView.setImageDrawable(ContextCompat.getDrawable(TestingConnection.this, R.drawable.ic_error));
                    button_1.setText(getResources().getString(R.string.exit));
                    stopTestingConnection();

                } else if(test == LOCATION_PERMISSION){
                    titleView.setText(getResources().getString(R.string.onboarding_location_check_dialog_title));
                    caption_view.setText(getResources().getString(R.string.onboarding_location_check_dialog_msg));
                    animationView.setImageDrawable(ContextCompat.getDrawable(TestingConnection.this, R.drawable.ic_error));
                    button_1.setText(getResources().getString(R.string.exit));
                    stopTestingConnection();
                }
            }
            finally {
                mHandler.postDelayed(mTestingConnection, mInterval);
            }
        }
    };

    void stopTestingConnection(){

        mHandler.removeCallbacksAndMessages(null);
        //if(android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.P)
            //cm.unregisterNetworkCallback(networkCallback);
        //mHandler.removeCallbacks(mTestingConnection);
    }

    private int testConnection() {
        count = count + 1;
        Context context = TestingConnection.this;
        String eduroam_old = "\"eduroam\"";
        String eduroam_new = "eduroam";

        if (Helper.hasMobileInternetConnection(context)) return CONNECTED_MOBILE_NETWORK;

        // Testing for Wifi Connection
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm == null) return EDUROAM_NO_INTERNET;



        if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {
            NetworkCapabilities capabilities = cm.getNetworkCapabilities(cm.getActiveNetwork());
            if (capabilities != null) {
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {

                    WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                    String ssid = (wifiManager != null) ? wifiManager.getConnectionInfo().getSSID() : "null";
                    //Log.d("TestConnection", "Wifi Manager: " + ssid);

                    NetworkRequest request = new NetworkRequest.Builder()
                            .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
                            .build();

                    networkCallback = new ConnectivityManager.NetworkCallback() {
                        @Override
                        public void onAvailable(Network network) {
                           //Log.d("TestConnection", network.toString());
                        }

                        @Override
                        public void onCapabilitiesChanged(Network network, NetworkCapabilities networkCapabilities) {
                            WifiInfo wifiInfo = (WifiInfo) networkCapabilities.getTransportInfo();
                            connected_ssid = (wifiInfo != null) ? wifiInfo.getSSID() : "null";
                            //Log.d("TestConnection", networkCapabilities.toString());

                        }
                    };

                    cm.requestNetwork(request, networkCallback);

                    return (connected_ssid.equals(eduroam_new) || connected_ssid.equals(eduroam_old) ||
                            ssid.equals(eduroam_new) || ssid.equals(eduroam_old) ?
                            CONNECTED : CONNECTED_TO_OTHER_NETWORK);
                }
            } else
                return EDUROAM_NO_INTERNET;

            return EDUROAM_NO_INTERNET;
        }
        else {
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            if (activeNetwork != null && activeNetwork.isConnectedOrConnecting()) {

                WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                String ssid = (wifiManager != null) ? wifiManager.getConnectionInfo().getSSID() : "null";

                return (ssid.contains(eduroam_old) || ssid.contains(eduroam_new)) ? CONNECTED : CONNECTED_TO_OTHER_NETWORK;

            } else {
                lastAddedWifi = Helper.readSharedPreferences(this, Helper.SHARED_PREF_LAST_ADDED_WIFI, "-1");
                WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);

                if (wifi == null) return EDUROAM_NO_INTERNET;

                boolean res = wifi.enableNetwork(Integer.parseInt(lastAddedWifi), true);
                Log.d("TestingConnection", "enableNetwork: " + res);
                if (res) {
                    cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo info = cm != null ? cm.getActiveNetworkInfo() : null;

                    if (info != null && info.isConnected()) {
                        Log.v(TAG, "Wifi Info " + info + " " + info.isConnected());
                        return Helper.hasInternet(TestingConnection.this) ? CONNECTED : EDUROAM_NO_INTERNET;
                    } else
                        return EDUROAM_NETWORK_NOT_FOUND;
                } else
                    return EDUROAM_NETWORK_NOT_ENABLED;

            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopTestingConnection();
    }

}

