package com.aadi.tucwlan.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;

import com.aadi.tucwlan.BuildConfig;
import com.aadi.tucwlan.R;

public class About extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        ((LinearLayout) findViewById(R.id.version_layout)).setVisibility(View.VISIBLE);
        ((AppCompatTextView) findViewById(R.id.version_no)).setText(BuildConfig.VERSION_NAME);

    }
}
