package com.aadi.tucwlan.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aadi.tucwlan.BuildConfig;
import com.aadi.tucwlan.R;
import com.aadi.tucwlan.adapter.DiagnosticAdapter;
import com.aadi.tucwlan.model.ConfigurationResponse;
import com.aadi.tucwlan.model.TickItems;
import com.aadi.tucwlan.utils.Helper;
import com.aadi.tucwlan.utils.WifiConfig;

import java.util.ArrayList;
import java.util.List;

public class DiagnosticActivity extends AppCompatActivity implements View.OnClickListener {

    private List<TickItems> mTickItems;
    private RecyclerView mRecyclerView;
    private NetworkChangeReceiver networkChangeReceiver = null;
    private AppCompatButton btn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.diagnostic_activity);

        mRecyclerView = findViewById(R.id.recyclerView);

        btn = (AppCompatButton) findViewById(R.id.button_title);
        btn.setText(getString(R.string.start_connection_test));
        btn.setOnClickListener(this);

        DiagnosticAdapter adapter = new DiagnosticAdapter(this, getItems());

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(adapter);

    }

    private List<TickItems> getItems() {
        mTickItems = new ArrayList<>();

        TickItems items = new TickItems();
        items.setSection_name(getString(R.string.Permissions));
        items.setTitle(getString(R.string.location_access));
        if(Helper.hasLocationPermission(this)){
            items.setSubtitle(getString(R.string.text_allowed));
            items.setCorrect(true);
        } else {
            items.setSubtitle(getString(R.string.text_blocked));
            items.setCorrect(false);
            items.setClickable(true);
            items.setAction("location_dialog_box");
        }
        mTickItems.add(items);

        getAppRequirementsData();
        getVersionData();
        return mTickItems;
    }

    private void getAppRequirementsData() {
        TickItems items = new TickItems();
        items.setSection_name(getString(R.string.onboarding_screen4_title));
        items.setTitle(getString(R.string.tick_category_connected_to_internet));
        if(Helper.hasInternet(this)){
            items.setSubtitle(getString(R.string.tick_msg_connected));
            items.setCorrect(true);
        } else {
            items.setSubtitle(getString(R.string.tick_msg_disconnected));
            items.setCorrect(false);
            items.setClickable(true);
            items.setAction("internet_dialog_box");
        }
        mTickItems.add(items);

        TickItems items1 = new TickItems();
        items1.setTitle(getString(R.string.tick_category_lock_screen));
        if(Helper.hasLockScreenSet(this)){
            items1.setSubtitle(getString(R.string.msg_enabled));
            items1.setCorrect(true);
        } else {
            items1.setSubtitle(getString(R.string.msg_disabled));
            items1.setCorrect(false);
            items1.setClickable(true);
            items1.setAction("screen_lock_dialog_box");
        }
        mTickItems.add(items1);

        TickItems items2 = new TickItems();
        items2.setTitle(getString(R.string.tick_category_wifi_enabled));

        WifiManager wifi = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        assert wifi != null;
        if (wifi.isWifiEnabled()) {
            items2.setSubtitle(getString(R.string.msg_enabled));
            items2.setCorrect(true);
        } else {
            items2.setSubtitle(getString(R.string.msg_disabled));
            items2.setCorrect(false);
            items2.setClickable(true);
            items2.setAction("internet_dialog_box");
        }
        mTickItems.add(items2);

    }

    private void getVersionData() {
        mTickItems.add(new TickItems(getString(R.string.android_version), "Android " + Build.VERSION.RELEASE + "  (API " + Build.VERSION.SDK_INT + ")", true, getString(R.string.tick_heading_device)));
        mTickItems.add(new TickItems(getString(R.string.device_name), Build.BRAND + " " + Build.MODEL + " ", true));
        mTickItems.add(new TickItems(getString(R.string.app_version), BuildConfig.VERSION_NAME + " ", true));
    }

    @Override
    public void onResume() {
        super.onResume();
        updateLayout();

        IntentFilter networkChangeReceiverIntentFilter = new IntentFilter();
        networkChangeReceiverIntentFilter.addAction(WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION);
        networkChangeReceiverIntentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);

        this.networkChangeReceiver = new NetworkChangeReceiver();
        Log.d("networkChange_broadcast", "register receiver");
        this.registerReceiver(this.networkChangeReceiver, networkChangeReceiverIntentFilter);
    }

    private void updateLayout() {
        mRecyclerView.removeAllViews();
        mRecyclerView.setAdapter(new DiagnosticAdapter(this, getItems()));
    }

    @Override
    public void onStop() {
        super.onStop();
        this.unregisterReceiver(this.networkChangeReceiver);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.button_title){
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {
                if(Helper.hasLocationPermission(DiagnosticActivity.this)) {
                    checkforInternet();
                } else
                    Helper.showLocationPermissionDialog(DiagnosticActivity.this);
            } else {
                checkforInternet();
            }
        }

    }

    private void checkforInternet() {
        if (Helper.hasInternet(DiagnosticActivity.this)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(DiagnosticActivity.this);
            builder.setTitle(R.string.testing_connection_title);
            builder.setMessage(R.string.testing_connection_alert_msg);
            builder.setPositiveButton(R.string.test, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    startActivity(new Intent(DiagnosticActivity.this, TestingConnection.class));
                }
            });
            builder.setNegativeButton(R.string.cancel, null);
            builder.show();
        } else
            Helper.showCheckInternetDialog(DiagnosticActivity.this);
    }

    private class NetworkChangeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Log.d("networkChange_broadcast", "Get action: " + intent.getAction());
                updateLayout();
            } catch (Exception exception) {
                Log.d("networkChange_broadcast", "Error while recompute ticklist", exception);
            }
        }
    }
}
