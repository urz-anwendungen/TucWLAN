package com.aadi.tucwlan.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.aadi.tucwlan.R;
import com.aadi.tucwlan.utils.Helper;

public class OnBoardingLastFragment extends Fragment {

    public static Boolean isConnected = false;
    public static Boolean isSecure = false;
    public static Boolean hasLocationPermission = false;
    private AppCompatCheckBox checkbox1;
    private AppCompatCheckBox checkbox2;
    private AppCompatCheckBox checkbox4;

    private NetworkChangeReceiver networkChangeReceiver = null;
    private FragmentActivity mAct;
    private static Boolean hasBackgroundRunPermission;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        LinearLayout mRootView = (LinearLayout) inflater.inflate(R.layout.fragment_onboarding_last, container, false);
        mAct = getActivity();

        checkbox1 = (AppCompatCheckBox) mRootView.findViewById(R.id.checkbox1);
        checkbox2 = (AppCompatCheckBox) mRootView.findViewById(R.id.checkbox2);
        checkbox4 = (AppCompatCheckBox) mRootView.findViewById(R.id.checkbox4);

        updateLayout();

        checkbox1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(!Helper.hasLockScreenSet(mAct)){
                    Helper.showCheckLockScreenDialog(compoundButton);
                } else
                    isSecure = b;
            }
        });
        checkbox2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(!Helper.hasInternet(mAct)){
                    Helper.showCheckInternetDialog(compoundButton);
                } else
                    isConnected = b;
            }
        });
        checkbox4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(!Helper.hasPermissionToRunInBackground(mAct)){
                    Helper.showCheckBackgroundPermission(compoundButton);
                } else {
                    checkbox4.setChecked(true);
                    checkbox4.setEnabled(false);
                }
            }
        });

        return mRootView;
    }

    private void updateLayout(){

        isSecure = Helper.hasLockScreenSet(mAct);
        checkbox1.setChecked(isSecure);

        isConnected = Helper.hasInternet(mAct);
        checkbox2.setChecked(isConnected);

        hasBackgroundRunPermission = Helper.hasPermissionToRunInBackground(mAct);
        checkbox4.setChecked(hasBackgroundRunPermission);
    }

    @Override
    public void onStop() {
        super.onStop();
        mAct.unregisterReceiver(this.networkChangeReceiver);
    }

    public static Fragment newInstance() {
        return new OnBoardingLastFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        updateLayout();

    }

    @Override
    public void onStart() {
        super.onStart();
        updateLayout();

        IntentFilter networkChangeReceiverIntentFilter = new IntentFilter();
        networkChangeReceiverIntentFilter.addAction(WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION);
        networkChangeReceiverIntentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);

        this.networkChangeReceiver = new NetworkChangeReceiver();
        Log.d("networkChange_broadcast", "register receiver");
        mAct.registerReceiver(this.networkChangeReceiver, networkChangeReceiverIntentFilter);
    }

    private class NetworkChangeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Log.d("networkChange_broadcast", "Get action: " + intent.getAction());
                updateLayout();
            } catch (Exception exception) {
                Log.d("networkChange_broadcast", "Error while recompute ticklist", exception);
            }
        }
    }
}
