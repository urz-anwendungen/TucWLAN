package com.aadi.tucwlan.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.aadi.tucwlan.R;

public class OnBoardingFragment extends Fragment {

    private static final String SECTION_NUMBER = "section_number";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        LinearLayout mRootView = (LinearLayout) inflater.inflate(R.layout.fragment_onboarding, container, false);

        int pos = getArguments().getInt(SECTION_NUMBER);
        String[] title_text = getResources().getStringArray(R.array.onboarding_title);
        String[] subtitle_text = getResources().getStringArray(R.array.onboarding_subtitle);


        ((TextView) mRootView.findViewById(R.id.onboarding_title)).setText(title_text[pos]);
        ((TextView) mRootView.findViewById(R.id.onboarding_subtitle)).setText(subtitle_text[pos]);

        return mRootView;
    }

    public static Fragment newInstance(int pos) {
        OnBoardingFragment fragment = new OnBoardingFragment();
        Bundle args = new Bundle();
        args.putInt(SECTION_NUMBER, pos);
        fragment.setArguments(args);
        return fragment;
    }
}
