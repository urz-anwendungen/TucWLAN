package com.aadi.tucwlan.model;

import com.google.gson.annotations.SerializedName;

import java.security.cert.X509Certificate;

public class ConfigurationResponse {

     @SerializedName("type")
     private String type;
     @SerializedName("version")
     private int version;
     @SerializedName("ssid")
     private String ssid;
     @SerializedName("identity")
     private String identity;
     @SerializedName("eapMethod")
     private String eapMethod;
     @SerializedName("phase2")
     private String phase2;
     @SerializedName("anonymousIdentity")
     private String anonymousIdentity;
     @SerializedName("subjectMatch")
     private String subjectMatch;
     @SerializedName("deploy_status_code")
     private int deployStatusCode;
     @SerializedName("certs")
     private X509Certificate[] certs;
     @SerializedName("pk")
     private int pk;
     @SerializedName("username")
     private String username;
     @SerializedName("device_name")
     private String deviceName;
     @SerializedName("password")
     private String password;
     @SerializedName("deploy_status")
     private String deployStatus;

    private String certificateStatus;

    public ConfigurationResponse(){

     }

    public ConfigurationResponse(String type, int version, String ssid, String identity, String eapMethod, String phase2, String anonymousIdentity, String subjectMatch, int deployStatusCode, X509Certificate[] certs, int pk, String username, String deviceName, String password, String deployStatus) {
        this.type = type;
        this.version = version;
        this.ssid = ssid;
        this.identity = identity;
        this.eapMethod = eapMethod;
        this.phase2 = phase2;
        this.anonymousIdentity = anonymousIdentity;
        this.subjectMatch = subjectMatch;
        this.deployStatusCode = deployStatusCode;
        this.certs = certs;
        this.pk = pk;
        this.username = username;
        this.deviceName = deviceName;
        this.password = password;
        this.deployStatus = deployStatus;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    public String getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = identity;
    }

    public String getEapMethod() {
        return eapMethod;
    }

    public void setEapMethod(String eapMethod) {
        this.eapMethod = eapMethod;
    }

    public String getPhase2() {
        return phase2;
    }

    public void setPhase2(String phase2) {
        this.phase2 = phase2;
    }

    public String getAnonymousIdentity() {
        return anonymousIdentity;
    }

    public void setAnonymousIdentity(String anonymousIdentity) {
        this.anonymousIdentity = anonymousIdentity;
    }

    public String getSubjectMatch() {
        return subjectMatch;
    }

    public void setSubjectMatch(String subjectMatch) {
        this.subjectMatch = subjectMatch;
    }

    public int getDeployStatusCode() {
        return deployStatusCode;
    }

    public void setDeployStatusCode(int deployStatusCode) {
        this.deployStatusCode = deployStatusCode;
    }

    public X509Certificate[] getCerts() {
        return certs;
    }

    public void setCerts(X509Certificate[] certs) {
        this.certs = certs;
    }

    public int getPk() {
        return pk;
    }

    public void setPk(int pk) {
        this.pk = pk;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDeployStatus() {
        return deployStatus;
    }

    public void setDeployStatus(String deployStatus) {
        this.deployStatus = deployStatus;
    }

    public String getCertificateStatus() {
        return certificateStatus;
    }

    public void setCertificateStatus(String certificateStatus) {
        this.certificateStatus = certificateStatus;
    }


}
